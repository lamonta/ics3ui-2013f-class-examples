
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class Pong extends JPanel implements KeyListener{

    int x1 = 40;
    int y1 = 250;
    
    int x2 = 730;
    int y2 = 250;
    
    int paddleWidth = 20;
    int paddleHeight = 100;
    
    boolean up1, up2, down1, down2 = false;
    int speed = 5;
    
    //ball variables
    int x = 390;
    int y = 290;
    int dx = 1;
    int dy = 1;
    int bSpeed = 5;
    int size = 20;
    
    // game score
    int player1, player2 = 0;
    
    // logic goes here
    public void run()
    {
        boolean done = false;
        while(done == false)
        {
            // moving paddle 1
            if(up1 == true)
            {
                y1 = y1 - speed;
            }else if(down1 == true)
            {
                y1 = y1 + speed;
            }
            
            // moving paddle 2
            if(up2== true)
            {
                y2 = y2 - speed;
            }else if(down2 == true)
            {
                y2 = y2 + speed;
            }
            
            // limits paddle 1
            if(up1 == true && y1 <= 0)
            {
                up1 = false;
                y1 = 0;
            }else if(down1 == true && y1+paddleHeight >= getHeight())
            {
                down1 = false;
                y1 = getHeight() - paddleHeight;
            }
            
            // limits paddle 2 to screen
            if(up2 == true && y2 <= 0)
            {
                up2 = false;
                y2 = 0;
            }else if(down2 == true && y2+paddleHeight >= getHeight())
            {
                down2 = false;
                y2 = getHeight() - paddleHeight;
            }
            
            
            // move the ball
            x = x + dx*bSpeed;
            y = y + dy*bSpeed;
            
            if(y + size > getHeight())
            {
                dy = -1;
            }else if(x + size > getWidth())
            {
                dx = -1;
                player1++;
            }else if(y < 0)
            {
                dy = 1;
            }else if(x < 0)
            {
                dx = 1;
                player2++;
            }
            
            // ball bouncing off paddle 1
            int px = x;
            int py = y + size/2;
            if(px > x1 && px < x1 + paddleWidth
                    && py > y1 && py < y1 + paddleHeight)
            {
                dx = 1;
            }
            
            // ball bouncing off paddle 2
            px = x + size;
            py = y + size/2;
            if(px > x2 && px < x2 + paddleWidth
                    && py > y2 && py < y2 + paddleHeight)
            {
                dx = -1;
            }
            
            if(player1 == 10)
            {
                done = true;
            }else if(player2 == 10)
            {
                done = true;
            }
            
            repaint(); // updates screen
            try{
                Thread.sleep(20);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
        
    }
    
    // painting goes here
    public void paintComponent(Graphics g)
    {
        int width = getWidth();
        int height = getHeight();
        
        g.clearRect(0,0,800,600);
        g.fillRect(0,0,800,600);
        g.setColor(Color.WHITE);
        g.fillRect(x1,y1, paddleWidth,paddleHeight);
        g.fillRect(x2,y2,paddleWidth,paddleHeight);
        g.fillOval(x,y,size,size);
        
        // draw scores
        
        g.drawString(""+player1, 200, 100);
        g.drawString(""+player2, 600,100);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Pong Game"); // makes window
        frame.setSize(800,600); // sets window size
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Pong panel = new Pong(); // creates our game
        frame.add(panel); // adds our game to window
        frame.setVisible(true); // shows window
        frame.addKeyListener(panel); // links the key control
        
        panel.run(); // run the game
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode(); // get the pressed key
        // player 1
        if(key == KeyEvent.VK_W) // check for w
        {
            up1 = true;
        }else if(key == KeyEvent.VK_S) // check for s
        {
            down1 = true;
        }
        
        // player 2
        if(key == KeyEvent.VK_UP) // check for up arrow
        {
            up2 = true;
        }else if(key == KeyEvent.VK_DOWN) // check for down arrow
        {
            down2 = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode(); // get the pressed key
        // player 1
        if(key == KeyEvent.VK_W) // check for w
        {
            up1 = false;
        }else if(key == KeyEvent.VK_S) // check for s
        {
            down1 = false;
        }
        
        // player 2
        if(key == KeyEvent.VK_UP) // check for up arrow
        {
            up2 = false;
        }else if(key == KeyEvent.VK_DOWN) // check for down arrow
        {
            down2 = false;
        }
    }
}

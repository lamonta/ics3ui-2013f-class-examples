/**
 *
 * @author Adam
 */
public class StringManipulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String s = "Lamont";
        String s2 = "bob";
        
        if(s.equals("lamont"))
        {
            System.out.println("The SAME");
        }else
        {
            System.out.println("Not THE SAME");
        }
        
        if(s.endsWith("ant"))
        {
            System.out.println("Good");
        }else
        {
            System.out.println("No good");
        }
        
        int spot = s.indexOf("z");
        System.out.println(spot);
        
        
        s = s.replace("m","");
        s2 = s2.replace("b", "");
        System.out.println(s);
        
        s = "Lamont";
        String str = s.substring(1);
        str = str + s.substring(0,1);
        str = str + "ay";
        System.out.println(str);
        
    }
}


import java.util.Scanner;

/*
 * Demonstrates how to do if statements
 */
/**
 * @author Adam
 */
public class IfStatementExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number");
        int num = input.nextInt();
        System.out.println("Please enter another number");
        int num2 = input.nextInt();
        
        if(num < num2)
        {
            
        }
        
        if(num >= 0 && num <= 10)
        {
            System.out.println("Thats between 0 and 10");
        }
        else if(num >= 50 && num <= 100)
        {
            System.out.println("Thats a pretty big number");
        }
        else if(num > 1000)
        {
            System.out.println("Thats a really big number");
        }
        else
        {
            System.out.println("I don't like that number");
        }
        System.out.println("End of program");
        input.close();
    }
}

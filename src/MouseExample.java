
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Adam
 */
public class MouseExample extends JPanel implements MouseListener, MouseMotionListener{

    int mx = -500;
    int my = -500;
    int rx = -500;
    int ry = -500;
    
    boolean pressed = false;
    
    int[] xPoints = new int[100];
    int[] yPoints = new int[100];
    int numPoints = 0;
    
    public void run()
    {
        while(true)
        {
            
            
            repaint();
            try
            {
                Thread.sleep(10);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
    }
    
    public void paintComponent(Graphics g)
    {
        g.clearRect(0,0,800,600);
        g.drawOval(mx,my,20,20);
        for(int i = 0; i < numPoints; i ++)
        {
           g.fillOval(xPoints[i], yPoints[i], 20, 20); 
        }
        // draws a line when i release the mouse
        g.setColor(Color.red);
        if(pressed == false)
        {
            g.drawLine(mx+10, my+10, rx, ry);
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Mouse Example");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        MouseExample panel = new MouseExample();
        panel.addMouseListener(panel);
        panel.addMouseMotionListener(panel);
        frame.add(panel);
        
        frame.setVisible(true);
        frame.setResizable(false);
        panel.run();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
     

    }

    @Override
    public void mousePressed(MouseEvent e) {
        pressed = true;
        mx = e.getX();
        my = e.getY();
        // save the spot we have clicked on
        // add on more spot to draw
        xPoints[numPoints] = mx;
        yPoints[numPoints] = my;
        numPoints++;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        pressed = false;
        rx = e.getX();
        ry = e.getY();
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mx = e.getX();
        my = e.getY();
        
    }
}

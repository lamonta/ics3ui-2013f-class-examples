
import java.util.Random;
import java.util.Scanner;

/**
 * Show a while loop in a game
 * @author Adam
 */
public class NumberGuessingGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        int number = generator.nextInt(100)+1;
        int guess = 0;
        int numGuesses = 0;
        
        do
        {
            System.out.println("Please guess a number between 1 and 100");
            guess = input.nextInt();
            numGuesses = numGuesses + 1;
            
            if(guess == number)
            {
                System.out.println("Congratulations. You win!");
            }else if(numGuesses == 10)
            {
                System.out.println("That was your las guess!");
            }
            else if(guess > number)
            {
                System.out.println("You're too high!");
            }else if(guess < number)
            {
                System.out.println("You're too low");
            }
        }while(guess != number  &&  numGuesses < 10);
    }
}

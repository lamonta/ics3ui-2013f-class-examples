
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class GravityExample extends JPanel implements KeyListener{

    int x = 200;
    int y = 450;
    int dx = 0;
    int dy = 0;
    
    int gravity = 2;
    
    boolean jump = false;
    boolean up,right,left = false;
    
    public void run()
    {
        while(true)
        {
            

             // simulate moving x
            x = x + dx;
            if(!(x > 400 + 100 || x + 50 < 400 || y > 450 + 50 || y+50 < 450) )
            {
                if(dx > 0)
                {
                    x = 400 - 50 - 1;
                }
                if(dx < 0)
                {
                    x = 400 + 100 + 1;
                }
            }

            
            // gravity changing dy
            dy = dy + gravity;
            if( dy >= 30)
            {
                dy = 30;
            }
            // smiulate move y
            y = y + dy;

            // hit the block
            if(!(x > 400 + 100 || x + 50 < 400 || y > 450 + 50 || y+50 < 450) )
            {
                jump = false;
                y = 450 - 50 - 1;
                dy = 0;
            }
            // if bottom of char hits the floor(500)
            // reset the y coordinate
            if(y + 50 >= 500)
            {
                y = 500 -50;
                jump = false;
                dy = 0;
            }
            
           
            
            
            if(right == true)
            {
                dx = 5;
            }
            if(left == true)
            {
                dx = -5;
            }
            if(up == true && jump == false)
            {
                jump = true;
                dy = -30; // start off jumping
            }
            
            if(right == false && left == false)
            {
                dx = 0;
            }
            
            repaint();
            try
            {
                Thread.sleep(20);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public void paintComponent(Graphics g)
    {
        g.clearRect(0, 0, 800, 600);
        g.setColor(Color.GREEN);
        g.fillRect(0,500,800,100);
        g.setColor(Color.BLACK);
        g.fillRect(x,y,50,50);
        g.setColor(Color.yellow);
        g.fillRect(400,450,100,50);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Gravity Example");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        GravityExample panel = new GravityExample();
        frame.add(panel);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.addKeyListener(panel);
        
        panel.run();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
       int key = e.getKeyCode();
       if(key == KeyEvent.VK_RIGHT)
       {
           right = true;
       }
       if(key == KeyEvent.VK_LEFT)
       {
           left = true;
       }
       if(key == KeyEvent.VK_UP)
       {
           up = true;

       }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
       if(key == KeyEvent.VK_RIGHT)
       {
           right = false;
       }
       if(key == KeyEvent.VK_LEFT)
       {
           left = false;
       }
       if(key == KeyEvent.VK_UP)
       {
           up = false;
       }
    }
}


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class AnimationExample extends JPanel implements KeyListener{
 
    BufferedImage spriteSheet1 = ImageHelper.loadImage("spriteSheet.png");
    BufferedImage spriteSheet = ImageHelper.makeColorTransparent(spriteSheet1, Color.BLACK);
    BufferedImage[] framesR = ImageHelper.splitImage(spriteSheet, 6, 1);
    BufferedImage[] framesL = new BufferedImage[6];
    
    BufferedImage STANDR = framesR[3];
    BufferedImage STANDL;
    BufferedImage characterPic = STANDR;
    
    int frameNum = 0;
    
    boolean up, down, left, right = false;
    int x = 10;
    int y = 300;
    int speed = 8;
    boolean facingLeft = false;
    
    public void run()
    {
        for(int i = 0; i < 6; i++)
        {
            framesL[i] = ImageHelper.horizontalflip(framesR[i]);
        }
        
        STANDL = framesL[3];
        
        while(true)
        {
            if(up == true)
            {
                y = y - speed;
            }else if(down == true)
            {
                y = y + speed;
            }
            
            if(right == true)
            {
                x = x + speed;
                characterPic = framesR[frameNum];
                facingLeft = false;
            }else if(left == true)
            {
                x = x - speed;
                characterPic = framesL[frameNum];
                facingLeft = true;
            }
            
            // standing still
            if(up == false && down == false && right == false && left == false)
            {
                if(facingLeft == true)
                {
                    characterPic = STANDL;
                }else
                {
                    characterPic = STANDR;
                }
                frameNum = 0;
            }else
            {
                frameNum = (frameNum + 1) % 6;
            }
            
            repaint();
            try
            {
                Thread.sleep(40);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public void paintComponent(Graphics g)
    {
        g.clearRect(0,0,800,600);
        g.drawImage(characterPic, x, y, null);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Animation");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        AnimationExample panel = new AnimationExample();
        frame.add(panel);
        frame.addKeyListener(panel);
        
        frame.setVisible(true);
        frame.setResizable(false);
        
        panel.run();
    }

    @Override
    public void keyTyped(KeyEvent e) {
       
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_UP)
        {
            up = true;
        }else if(key == KeyEvent.VK_DOWN)
        {
            down = true;
        }else if(key == KeyEvent.VK_RIGHT)
        {
            right = true;
        }else if(key == KeyEvent.VK_LEFT)
        {
            left = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_UP)
        {
            up = false;
        }else if(key == KeyEvent.VK_DOWN)
        {
            down = false;
        }else if(key == KeyEvent.VK_RIGHT)
        {
            right = false;
        }else if(key == KeyEvent.VK_LEFT)
        {
            left = false;
        }
    }
}

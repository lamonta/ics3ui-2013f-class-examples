
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class KeyboardExample extends JPanel implements KeyListener{

    int x = 200;
    int y = 200;
    
    boolean right = false;
    boolean left = false;
    boolean up = false;
    boolean down = false;
    
    // draw things here
    public void paintComponent(Graphics g)
    {
        g.clearRect(0,0,800,600);
        g.setColor(Color.RED);
        g.fillOval(x, y, 40, 40);
    }
    
    // game logic here
    public void run()
    {
        while(true)
        {
            if(up == true)
            {
                y = y - 1;
            }else if(down == true)
            {
                y = y + 1;
            }
            
            if(right == true)
            {
                x = x + 1;
            }else if(left == true)
            {
                x = x - 1;
            }
            
            repaint();
            try
            {
                Thread.sleep(20);
            }catch(Exception e)
            {
                
            }
        }
    }
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Key Control");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        KeyboardExample panel = new KeyboardExample();
        frame.add(panel);
        
        
        frame.addKeyListener(panel);
        frame.setVisible(true);
        
        panel.run();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_W)
        {
            up = true;
        }
        if(key == KeyEvent.VK_S)
        {
            down = true;
        }
        
        if(key == KeyEvent.VK_A)
        {
            left = true;
        }
        if(key == KeyEvent.VK_D)
        {
            right = true;
        }
       
    }

    @Override
    public void keyReleased(KeyEvent e) {
         int key = e.getKeyCode();
        if(key == KeyEvent.VK_W)
        {
            up = false;
        }
        if(key == KeyEvent.VK_S)
        {
            down = false;
        }
        
        if(key == KeyEvent.VK_A)
        {
            left = false;
        }
        if(key == KeyEvent.VK_D)
        {
            right = false;
        }
    }
}

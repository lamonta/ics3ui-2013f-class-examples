
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class MouseMotion extends JPanel implements MouseMotionListener, MouseListener{

    int mx = -500;
    int my = -500;
    
    int buttonX = 200;
    int buttonY = 300;
    int height = 30;
    int width = 100;
    boolean highlight = false;
    
    int level = 0;
    
    
    public void run()
    {
        while(true)
        {
            if(level == 0)
            {
                if(mx > buttonX && mx < buttonX + width
                        && my > buttonY && my < buttonY + height)
                {
                    highlight = true;
                }else
                {
                    highlight = false;
                }
            }
            
            
            repaint();
            try
            {
                Thread.sleep(10);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public void paintComponent(Graphics g)
    {
        g.clearRect(0,0,800,600);
        String s = "(" + mx + ", " + my + ")";
        g.drawString(s, 0, 20);
        
        g.setColor(Color.GREEN);
        g.fillOval(mx-10, my-10, 20, 20);
        
        if(level == 0)
        {
            // draw a button
            g.setColor(Color.RED);
            if(highlight == true)
            {
                g.fillRect(buttonX, buttonY, width, height);
            }else
            {
                g.drawRect(buttonX, buttonY, width, height);
            }
        }else if(level == 1)
        {
            g.setColor(Color.BLUE);
            g.fillOval(400,300,30,50);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Mouse Motion Example");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // create panel and add listener
        MouseMotion panel = new MouseMotion();
        panel.addMouseMotionListener(panel);
        panel.addMouseListener(panel);
        
        // set up rest of frame
        frame.add(panel);
        frame.setVisible(true);
        frame.setResizable(false);
        
        panel.run(); // run program
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mx = e.getX();
        my = e.getY();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if(level == 0 && mx > buttonX && mx < buttonX + width
                    && my > buttonY && my < buttonY + height)
        {
            level = 1;
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
        
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }
}

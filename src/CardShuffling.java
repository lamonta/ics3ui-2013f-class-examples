
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class CardShuffling {

    // this method takes in a number representing a card
    // and will return the associated value of it
    public static void shuffle(int[] deck)
    {
                // shuffle cards
        Random rand = new Random();
        for(int i=0; i < deck.length-1; i++)
        {
            int k = rand.nextInt(52-(i+1)) + (i+1);
            int temp = deck[i];
            deck[i] = deck[k];
            deck[k] = temp;
        }
    }
    
    
    public static int cardValue(int num)
    {
        int rank = num % 13 + 1;
        
        if(rank == 1) // ace
        {
            return 11;
        }else if(rank == 11) // jack
        {
            return 10;
        }else if (rank == 12)// queen
        {
            return 10;
        }else if (rank == 13)// king
        {
            return 10;
        }else
        {
            return rank;
        }
    }
    
    
    // this method will take in an integer that
    // represents a card, and print out its name
    public static void printCard(int num)
    {
        int rank = num%13 + 1;
        int suit = num/13;
        
        String s = "";
        if(rank >= 2 && rank <= 10)
        {
            s = s + rank;
        }else if(rank == 1)
        {
            s = "Ace";
        }else if (rank == 11)
        {
            s = "Jack";
        }else if (rank == 12)
        {
            s = "Queen";
        }else if (rank == 13)
        {
            s = "King";
        }
        
        s = s+ " of ";
        
        if(suit == 0)
        {
            s = s + "Clubs";
        }else if(suit == 1)
        {
            s = s + "Diamonds";
        }else if(suit == 2)
        {
            s = s + "Hearts";
        }else if(suit == 3)
        {
            s = s + "Spades";
        }
        
        System.out.println(s);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // creating an array to represent cards
        int[] cards = new int[52];
        for(int i=0; i < cards.length; i++)
        {
            cards[i] = i;
        }
        
        //shuffle the cards
        shuffle(cards);
        
        // print out cards
        for(int c: cards)
        {
            printCard(c);
            int value = cardValue(c);
            System.out.println("Worth " + value + " points");
        }
    }
}


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class GraphicsExample extends JPanel{
    
    int x = 200;
    int y = 200;
    int dx = 1;
    int dy = 1;
    int width = 50;
    int dw = 1;
    
    
    // painting things go here
    public void paintComponent(Graphics g)
    {
        
//        g.clearRect(0, 0, 800, 600);
//        
//        g.drawLine(10,50,500,200);
//        
//        g.setColor(Color.BLACK);
//        g.drawRect(100, 500, 400,50);
//        
//        // custom color(red,green,blue)
//        // max for each colour is 255
//        Color customColor = new Color(110,0,133);
//        g.setColor(customColor);
//        g.fillRect(0, 300, 400,50);

        g.clearRect(0,0,800,600);
        g.fillOval(x, y, width, 50);
        
//        int[] xs = {300, 500,200};
//        int[] ys = {200, 400, 300};
//        int n = 3;
//        
//        g.setColor(Color.RED);
//        g.fillPolygon(xs, ys, n);
//        
//        g.setColor(Color.BLUE);
//        g.fillArc(100, 300, 100, 75, 197, 123);
//        
        
    }
    
    // game logic goes here
    public void run()
    {
        while(true)
        {
            if(x < 0)
            {
                dx = +1;
                
            }else if(x + width > getWidth())
            {
                dx = -1;
              
            }
            
            if( y < 0)
            {
                dy = +1;
            }else if(y + 50 > getHeight())
            {
                dy = -1;
            }
            
            if (width > 50)
            {
                dw = -1;
            }if (width < 0)
            {
                dw = 1;
            }
            
            width = width + dw;
            x = x + dx;
            y = y + dy;
            
            repaint();
            
            try
            {
                Thread.sleep(10);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
     
    }
    
    // initial window setup
    public static void main(String[] args)
    {
        JFrame form = new JFrame("My Drawing");
        form.setSize(800,600);
        
        GraphicsExample panel = new GraphicsExample();
        form.add(panel);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        form.setVisible(true);
        form.setResizable(false);
        panel.run();
    }
    
}

/** Some Simple Loop Examples
 *
 * @author Adam
 */
public class WhileLoop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int count = 0;
        while(count <= 10)
        {
            System.out.println(count);
            count = count + 1;
        }
        System.out.println("I'm done");
    }
}

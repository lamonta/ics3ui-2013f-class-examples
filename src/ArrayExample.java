
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class ArrayExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[] nums = {10,-17,23,5,16};
        System.out.println("Before: " + nums[1]);
        nums[1] = 17;
        System.out.println("After: " + nums[1]);
        
        Scanner input = new Scanner(System.in);
        String[] names = new String[10];
//        System.out.println("Please enter 5 names on seperate lines");
//        names[0] = input.nextLine();
//        names[1] = input.nextLine();
//        names[2] = input.nextLine();
//        names[3] = input.nextLine();
//        names[4] = input.nextLine();
        for(int i=0; i < names.length; i++)
        {
            System.out.println("Please enter a name");
            names[i] = input.nextLine();
        }
        System.out.println("You entered the following names");
        for(int i=0; i < names.length; i++)
        {
            System.out.println(names[i]);
        }
        
        
        
    }
}
